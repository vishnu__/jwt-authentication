from django.conf.urls import url

from blog.views import TopicView, ArticleView, ArticleListView, ArticleUpdateView, ArticleDeleteView

urlpatterns = [
    url(r'^topic', TopicView.as_view()),
    url(r'^article', ArticleView.as_view()),
    url(r'^list-article', ArticleListView.as_view()),
    url(r'^update-article', ArticleUpdateView.as_view()),
    url(r'^delete-article', ArticleDeleteView.as_view()),
]
