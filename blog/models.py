import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _

from taggit.managers import TaggableManager
from taggit.models import GenericUUIDTaggedItemBase, TaggedItemBase


class UUIDTaggedItem(GenericUUIDTaggedItemBase, TaggedItemBase):
    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class Topic(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    image = models.ImageField(blank=False, null=False, upload_to='media/topics')
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Article(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    image = models.ImageField(blank=False, null=False, upload_to='media/articles')
    content = models.TextField()
    is_featured = models.BooleanField(default=False)
    count = models.PositiveIntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    tags = TaggableManager(through=UUIDTaggedItem, blank=True)

    def __str__(self):
        return self.title

    @property
    def topic_name(self):
        return self.topic.name
