from rest_framework import serializers
from taggit_serializer.serializers import (TagListSerializerField, TaggitSerializer)

from blog.models import Topic, Article


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ['id', 'name', 'image']
        extra_kwargs = {'id': {'read_only': True}}

    def create(self, validated_data):
        topic = Topic(**validated_data)
        topic.save()
        return topic


class ArticleSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField(allow_null=True)

    class Meta:
        model = Article
        fields = ['id', 'topic', 'title', 'image', 'content', 'is_featured', 'count', 'tags']
        extra_kwargs = {'id': {'read_only': True}, 'count': {'read_only': True}}

    def create(self, validated_data):
        tags = validated_data.pop('tags')
        article = Article(**validated_data)
        article.tags.add(*tags)
        article.save()
        return article


class ArticleListSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField()
    topic_name = serializers.ReadOnlyField()

    class Meta:
        model = Article
        fields = ['id', 'topic', 'title', 'is_featured', 'count', 'topic_name']


class ArticleUpdateSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField()
    tags = TagListSerializerField(allow_null=True)

    class Meta:
        model = Article
        fields = ['id', 'topic', 'title', 'content', 'is_featured', 'tags']
