import base64

from rest_framework import status
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView

from blog.models import Topic, Article
from blog.serializers import TopicSerializer, ArticleSerializer, ArticleUpdateSerializer, ArticleListSerializer
from user.permissions import IsAdmin


class TopicView(CreateAPIView):
    serializer_class = TopicSerializer
    permission_classes = [IsAuthenticated, IsAdmin]

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            status_code = status.HTTP_201_CREATED
            response = {
                'success': True,
                'status code': status_code,
                'id': serializer.data['id'],
                'message': 'Topic registered successfully',
            }

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        try:
            request.query_params.get('id')
            res = []
            if request.query_params.get('id') is None:
                topics = Topic.objects.all()
                serializer = self.serializer_class(topics, many=True)
                for _object in serializer.data:
                    _temp = _object
                    with open(_temp['image'], "rb") as f:
                        _temp['image'] = base64.b64encode(f.read())
                        _temp['name'] = _object['name']
                    res.append(_temp)
            else:
                topics = Topic.objects.get(id=request.query_params.get('id'))
                serializer = self.serializer_class(topics)
            status_code = status.HTTP_200_OK
            response = {
                'success': True,
                'status code': status_code,
                'result': serializer.data,
            }

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class ArticleView(APIView):
    serializer_class = ArticleSerializer
    permission_classes = [IsAuthenticated, IsAdmin]

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            status_code = status.HTTP_201_CREATED
            response = {
                'success': True,
                'status code': status_code,
                'message': 'Article added successfully',
            }

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class ArticleListView(APIView):
    serializer_class = ArticleSerializer
    list_serializer = ArticleListSerializer
    permission_classes = [AllowAny]

    def get(self, request):
        try:
            print(request.user.is_authenticated, request.user.is_anonymous)
            if request.query_params.get('id') is not None:
                articles = Article.objects.get(id=request.query_params.get('id'))
                tags = list(articles.tags.all())
                if request.user.is_authenticated:
                    similar = Article.objects.filter(tags__name__in=tags).distinct()
                else:
                    similar = Article.objects.filter(tags__name__in=tags, is_featured=False).distinct()
                articles.count = articles.count + 1
                articles.save()
                serializer = self.serializer_class(articles)
                data = serializer.data
                with open(serializer.data['image'], "rb") as f:
                    data['image'] = base64.b64encode(f.read())
                _serializer = self.list_serializer(similar, many=True)
                status_code = status.HTTP_200_OK
                response = {
                    'success': True,
                    'status code': status_code,
                    'result': data,
                    'similar': _serializer.data,
                }
                return Response(response, status=status_code)
            elif request.query_params.get('topic') is not None:
                topic = Topic.objects.filter(id=request.query_params.get('topic'))
                if topic.exists():
                    if request.user.is_authenticated:
                        articles = Article.objects.filter(topic=topic[0])
                    else:
                        articles = Article.objects.filter(topic=topic[0], is_featured=False)
                    serializer = self.list_serializer(articles, many=True)
                else:
                    status_code = status.HTTP_200_OK
                    response = {
                        'success': True,
                        'status code': status_code,
                        'message': 'Topic id invalid',
                    }
                    return Response(response, status=status_code)
            else:
                if request.user.is_authenticated:
                    ordering = request.query_params.get('ordering')
                    if ordering is None:
                        articles = Article.objects.all()
                    else:
                        sort = ordering.split("%")
                        articles = Article.objects.all().order_by(*sort)
                else:
                    articles = Article.objects.filter(is_featured=False)
                serializer = self.list_serializer(articles, many=True)
            status_code = status.HTTP_200_OK
            response = {
                'success': True,
                'status code': status_code,
                'result': serializer.data,
            }

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class ArticleUpdateView(UpdateAPIView):
    serializer_class = ArticleUpdateSerializer
    permission_classes = [IsAuthenticated, IsAdmin]

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                data = serializer.data
                tags = serializer.data.pop('tags')
                Article.objects.filter(id=data['id']).update(topic=data['topic'], title=data['title'],
                                                             content=data['content'], is_featured=data['is_featured']
                                                             )
                article = Article.objects.get(id=data['id'])
                article.tags.clear()
                article.tags.add(*tags)
                status_code = status.HTTP_200_OK
                response = {
                    'success': True,
                    'status code': status_code,
                    'message': 'Article updated successfully',
                }
            else:
                status_code = status.HTTP_400_BAD_REQUEST
                response = serializer.errors

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class ArticleDeleteView(APIView):
    serializer_class = ArticleUpdateSerializer
    permission_classes = [IsAuthenticated, IsAdmin]

    def delete(self, request):
        try:
            article = Article.objects.filter(id=request.data['id'])
            article.delete()
            return Response({
                'message': 'Article deleted successfully'
            }, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)
