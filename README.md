# Ractangle Project
Ractangle backend server

## Ractangle Backend Server

## Contributors
* Vishnu P S



## Setup
* virtualenv venv -p python

* source ./venv/bin/activate
	
* pip install -r requirements.txt

* python manage.py makemigrations --settings=vumonics.settings

* python manage.py migrate --settings=vumonics.settings

* python manage.py createsuperuser

* python manage.py runserver --settings=vumonics.settings

* python manage.py runserver --settings=vumonics.settings 8000 --insecure

* python manage.py runserver --settings=vumonics.settings 0.0.0.0:8000  --> when on server

* python manage.py runserver --settings=vumonics.settings 0.0.0.0:8000 --insecure
