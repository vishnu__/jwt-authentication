from django.contrib.auth import authenticate
from rest_framework import serializers
from django.contrib.auth.models import update_last_login
from rest_framework_jwt.settings import api_settings

from user.models import UserProfile, User

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class UserSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(required=False, read_only=True)

    class Meta:
        model = UserProfile
        fields = ['name', 'age', 'gender', 'id']


class UserRegistrationSerializer(serializers.ModelSerializer):
    profile = UserSerializer(required=False)

    class Meta:
        model = User
        fields = ['email', 'password', 'is_admin', 'profile']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        if validated_data.get('is_admin') is not None:
            is_admin = validated_data.pop('is_admin')
            user = User.objects.create_admin(**validated_data)
        else:
            user = User.objects.create_user(**validated_data)
        UserProfile.objects.create(
            user=user,
            name=profile_data['name'],
            age=profile_data['age'],
            gender=profile_data['gender']
        )
        return user


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    is_admin = serializers.BooleanField(read_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    user_info = UserSerializer(read_only=True)

    def validate(self, data):
        email = data.get("email", None)
        password = data.get("password", None)
        user = authenticate(email=email, password=password)
        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )
        try:
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            update_last_login(None, user)
            user_info = UserProfile.objects.filter(user=user)
        except User.DoesNotExist:
            raise serializers.ValidationError(
                'User with given email and password does not exists'
            )
        return {
            'email': user.email,
            'is_admin': user.is_admin,
            'token': jwt_token,
            'user_info': UserSerializer(user_info[0]).data
        }
