from django.conf.urls import url
from rest_framework_jwt.views import verify_jwt_token, refresh_jwt_token

from user.views import UserRegistrationView, UserLoginView

urlpatterns = [
    url(r'^signup', UserRegistrationView.as_view()),
    url(r'^login', UserLoginView.as_view()),
    url(r'^refresh', refresh_jwt_token),
    url(r'^verify', verify_jwt_token),
]
