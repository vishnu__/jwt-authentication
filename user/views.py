from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from user.serializers import UserRegistrationSerializer, UserLoginSerializer


class UserRegistrationView(CreateAPIView):
    serializer_class = UserRegistrationSerializer
    permission_classes = [AllowAny]

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            status_code = status.HTTP_201_CREATED
            response = {
                'success': True,
                'status code': status_code,
                'message': 'User registered successfully',
            }

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class UserLoginView(RetrieveAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request):
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            response = {
                'success': True,
                'status code': status.HTTP_200_OK,
                'message': 'User logged in successfully',
                'is_admin': serializer.data['is_admin'],
                'token': serializer.data['token'],
                'user_info': serializer.data['user_info'],
            }
            status_code = status.HTTP_200_OK

            return Response(response, status=status_code)
        except Exception as e:
            return Response({
                'error': repr(e)
            }, status=status.HTTP_400_BAD_REQUEST)
